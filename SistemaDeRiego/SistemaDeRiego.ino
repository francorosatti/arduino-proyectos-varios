/************************************************************
 * Name: SistemaDeRiego.ino
 * Desc: Sistema de riego diario automatico
 * Auth: Franco Rosatti
 * Date: Oct-2019
 * Vesr: 1.0
 ************************************************************/

//******************************************************************************
// Librerias
//******************************************************************************
#include <avr/wdt.h>
#include <TimerOne.h>

//******************************************************************************
// Hardware
//******************************************************************************
#define GPIO_BOMBA 13

//******************************************************************************
// Definiciones
//******************************************************************************
#define TICKS_POR_SEGUNDO   10
#define SEGUNDOS_POR_MINUTO 60
#define MINUTOS_POR_HORA    60
#define HORAS_POR_DIA       24

//******************************************************************************
// Estructuras
//******************************************************************************
#define CMD_READ_TIME 1
#define CMD_WRITE_TIME 2
#define CMD_READ_PROC01_TIME 3
#define CMD_WRITE_PROC01_TIME 4

typedef struct structConfig {
  long command; // 1: GetTime, 2: SetTime, 3: GetProc1Time, 4: SetProc1Time
  long hour;
  long minute;
  long second;
} strCfg;

//******************************************************************************
// Proceso 01
//******************************************************************************
#define PROC01_EXEC_TIME_MS 5000 //Tiempo de ejecucion del proceso 1
bool proc01_start = false;
long proc01_count = 0;
bool proc01_enabled  = true;
long proc01_hour = 0;
long proc01_minute = 0;
long proc01_second = 3;

//******************************************************************************
// Contadores
//******************************************************************************
long tick_count = TICKS_POR_SEGUNDO;
long secs_count = SEGUNDOS_POR_MINUTO;
long mins_count = MINUTOS_POR_HORA;
long hour_count = HORAS_POR_DIA;

//******************************************************************************
// Configuracion
//******************************************************************************
strCfg configuracion;
strCfg response;

//******************************************************************************
// setup: Funcion de inicializacion
//******************************************************************************
void setup() {
  //Desabilito WDT
  wdt_disable();

  //Bluetooth
  Serial.begin(9600);   //Sets the baud for serial data transmission     
  
  //GPIO
  pinMode(GPIO_BOMBA, OUTPUT);
  //digitalWrite(GPIO_BOMBA, LOW);

  //Inicializo el Timer 1
  Timer1.initialize(100000); // Dispara cada 100 ms
  Timer1.attachInterrupt(ISR_Timer1Tick); // Activa la interrupcion y la asocia a ISR_Timer1Tick
  //Timer1.start();
  
  //Habilito WDT
  wdt_enable(WDTO_8S);
}

void ntohl(uint32_t* value) {
 uint32_t tmp_a = (*value & 0xff000000) >> 24;
 uint32_t tmp_b = (*value & 0x00ff0000) >> 8;
 uint32_t tmp_c = (*value & 0x0000ff00) << 8 ;
 uint32_t tmp_d = (*value & 0x000000ff) << 24;
 *value = tmp_d | tmp_c |tmp_b | tmp_a;
}

bool LeerComando() {
  if (Serial.readBytes((char*)&configuracion, sizeof(configuracion)) != sizeof(configuracion)) return false;
  ntohl((uint32_t*)&(configuracion.command));
  ntohl((uint32_t*)&(configuracion.hour));
  ntohl((uint32_t*)&(configuracion.minute));
  ntohl((uint32_t*)&(configuracion.second));
  //if(configuracion.second < 0 || configuracion.second > 59) return false; 
  //if(configuracion.minute < 0 || configuracion.minute > 59) return false;
  //if(configuracion.hour < 0 || configuracion.hour > 23) return false;
  return true;
}
bool EscribirComando(strCfg resp) {
  return (Serial.write((char*)&resp, sizeof(resp)) == sizeof(resp));
}

void Blink(int duration, int period){
  int period_counter = 0;
  int duration_counter = millis();
  while (millis() - duration_counter <= duration){
    digitalWrite(GPIO_BOMBA, digitalRead(GPIO_BOMBA) ^ 1);
    period_counter = millis();
    while (millis() - period_counter <= period){
      wdt_reset();
    }
  }
}
//******************************************************************************
// loop: Main del programa
//******************************************************************************
void loop() {
  wdt_reset();

  //Recibir configuracion
  if(Serial.available() >= sizeof(configuracion)) {
    if(LeerComando()){
      Timer1.stop();
      if (configuracion.command == CMD_READ_TIME) {
        response.command = CMD_READ_TIME;
        response.second = secs_count;
        response.minute = mins_count;
        response.hour = hour_count;
        EscribirComando(response);
      }
      else if(configuracion.command == CMD_WRITE_TIME) {
        Blink(3000,200);
        secs_count = configuracion.second;
        mins_count = configuracion.minute;
        hour_count = configuracion.hour;
        EscribirComando(configuracion);
      }
      else if(configuracion.command == CMD_READ_PROC01_TIME) {
        response.command = CMD_READ_PROC01_TIME;
        response.second = proc01_second;
        response.minute = proc01_minute;
        response.hour = proc01_hour;
        EscribirComando(response);
      }
      else if(configuracion.command == CMD_WRITE_PROC01_TIME) {
        proc01_second = configuracion.second;
        proc01_minute = configuracion.minute;
        proc01_hour = configuracion.hour;
        EscribirComando(configuracion);
      }
      else{
        Blink(3000,100);
      }
      Timer1.resume();
    }
    else{
      Serial.read(); //leo todo lo que haya y lo descarto
    }
  }

  wdt_reset();
  
  //Proceso 01: Bomba de Riego
  if(proc01_start) {
    proc01_start = false;
    //Enciendo Bomba
    digitalWrite(GPIO_BOMBA, HIGH);
    
    Blink(PROC01_EXEC_TIME_MS, 250);

    //Apago Bomba
    digitalWrite(GPIO_BOMBA, LOW);
  }
}

//******************************************************************************
// ISR_TimerTick: Interrupcion del Timer1
//******************************************************************************
void ISR_Timer1Tick() {
  tick_count++;
  if(tick_count >= TICKS_POR_SEGUNDO){
    tick_count = 0;
    secs_count++;

    //Tareas una vez por segundo
    if(proc01_enabled && hour_count == proc01_hour && mins_count == proc01_minute && secs_count == proc01_second) {
      proc01_start = true;
    }
  
    if(secs_count >= SEGUNDOS_POR_MINUTO) {
      secs_count = 0;
      mins_count++;

      if(mins_count >= MINUTOS_POR_HORA) {
        mins_count = 0;
        hour_count++;

        if(hour_count >= HORAS_POR_DIA) {
          hour_count = 0;
        }
      }
    }
  }
}
