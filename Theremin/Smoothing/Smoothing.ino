//**************************************************************************************************
// FUNCIONES
//**************************************************************************************************
void InitPromedio(int* readings, int len);
void Promedio(int* total, int* readings, int len, int* index, int* average, int input);
void Antirebote();

//**************************************************************************************************
// VARIABLES
//**************************************************************************************************
const int btn = 3;
const int trg = 5;
const int ech = 6;
const int pote1 = A0;
const int pote2 = A1;

int buttonState;
int lastButtonState = LOW;
unsigned long lastDebounceTime = 0;
unsigned long debounceDelay = 20;

const int numReadings1 = 10;
float readings1[numReadings1];      // the readings from the analog input
int readIndex1 = 0;              // the index of the current reading
float total1 = 0;                  // the running total
float average1 = 0;                // the average

const int numReadings2 = 10;
float readings2[numReadings2];      // the readings from the analog input
int readIndex2 = 0;              // the index of the current reading
float total2 = 0;                  // the running total
float average2 = 0;                // the average

//**************************************************************************************************
// PROGRAMA
//**************************************************************************************************
void setup() {
  pinMode(trg, OUTPUT); // Sets the trigPin as an Output
  pinMode(ech, INPUT); // Sets the echoPin as an Input
  digitalWrite(trg, LOW);

  pinMode(btn, INPUT);
  
  Serial.begin(9600); // Starts the serial communication

  InitPromedio(readings1, numReadings1);
}

void loop(){
  Antirebote();
  Promedio(&total1, readings1, numReadings1, &readIndex1, &average1, analogRead(pote1));
  //Promedio(&total2, readings2, numReadings2, &readIndex2, &average2, UltraSound()); //pasar a interrupcion

  tone(9, average1 + 31, 500);
  
  /* DOBLE TONO
   if(buttonState == HIGH){
    noTone(10);
    tone(9,average1 + 31, 500);
  }
  else{
    noTone(9);
    tone(10, 392);
    //tone(9,average1 + 31, 500);
  }
  */
}

void Antirebote(){
  int reading = digitalRead(btn);
  
  if (reading != lastButtonState) {
    // reset the debouncing timer
    lastDebounceTime = millis();
  }

  if ((millis() - lastDebounceTime) > debounceDelay) {
      buttonState = reading;
  }

  lastButtonState = reading;
}

void InitPromedio(float* readings, int len){
  for (int i = 0; i < len; i++) {
    *(readings + i) = 0.0f;
  }
}

void Promedio(float* total, float* readings, int len, int* index, float* average, float value){
  *total = *total - *(readings + *index);
  *(readings + *index) = value;
    
  *total = *total + *(readings + *index);
  *index = *index + 1;
  
  if (*index >= len) {
    *index = 0;
  }
  
  *average = *total / len;
}

float UltraSound(){
  // Sets the trigPin on HIGH state for 10 micro seconds
  digitalWrite(trg, HIGH);
  delayMicroseconds(10);
  digitalWrite(trg, LOW);
  
  // Reads the echoPin, returns the sound wave travel time in microseconds
  return pulseIn(ech, HIGH)*0.034/2;
}

