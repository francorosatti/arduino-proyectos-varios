// defines pins numbers
const int trigPinMati = 5;
const int echoPinMati = 6;

const float pi = 3.14159;

// defines variables
float durationMati;
float distanceMati;

int sensorPin = A0;    // select the input pin for the potentiometer

int sensorValue = 0;  // variable to store the value coming from the sensor

const int numReadings = 10;
int readings[numReadings];      // the readings from the analog input
int readIndex = 0;              // the index of the current reading
int total = 0;                  // the running total
int average = 0;                // the average


void setup() {
  pinMode(trigPinMati, OUTPUT); // Sets the trigPin as an Output
  pinMode(echoPinMati, INPUT); // Sets the echoPin as an Input
  
  Serial.begin(9600); // Starts the serial communication

  // Clears the trigPin
  digitalWrite(trigPinMati, LOW);
  delayMicroseconds(2);

  for (int thisReading = 0; thisReading < numReadings; thisReading++) {
    readings[thisReading] = 0;
  }
}

long i = 0;
float prom = 0;
void loop() {
  sensorValue = analogRead(sensorPin);
  prom = (prom + sensorValue) / 2;
  // Sets the trigPin on HIGH state for 10 micro seconds
  digitalWrite(trigPinMati, HIGH);
  delayMicroseconds(10);
  digitalWrite(trigPinMati, LOW);
  
  // Reads the echoPin, returns the sound wave travel time in microseconds
  durationMati = pulseIn(echoPinMati, HIGH);
  
  // Calculating the distance
  distanceMati = durationMati*0.034/2;

  float a = 0;
  
  if(distanceMati <= 50){
    a += distanceMati;
  }

  int b[10];
  
   
  tone(9, prom/4,2000);// + a*100);
  delay(500);

  Serial.println(prom/4);
  Serial.println(a*100);
  i += 1;
}
