// defines pins numbers
const int trigPinMati = 5;
const int echoPinMati = 6;

const int trigPinFran = 9;
const int echoPinFran = 10;

const float pi = 3.14159;

// defines variables
float durationMati;
float distanceMati;

float durationFran;
float distanceFran;

void setup() {
  pinMode(trigPinMati, OUTPUT); // Sets the trigPin as an Output
  pinMode(echoPinMati, INPUT); // Sets the echoPin as an Input
  pinMode(trigPinFran, OUTPUT); // Sets the trigPin as an Output
  pinMode(echoPinFran, INPUT); // Sets the echoPin as an Input
  
  Serial.begin(9600); // Starts the serial communication

  // Clears the trigPin
  digitalWrite(trigPinMati, LOW);
  digitalWrite(trigPinFran, LOW);
  delayMicroseconds(2);
}

long i = 0;

void loop() {
  // Sets the trigPin on HIGH state for 10 micro seconds
  digitalWrite(trigPinMati, HIGH);
  delayMicroseconds(10);
  digitalWrite(trigPinMati, LOW);
  
  // Reads the echoPin, returns the sound wave travel time in microseconds
  durationMati = pulseIn(echoPinMati, HIGH);
  
  // Calculating the distance
  distanceMati = durationMati*0.034/2;


  // Sets the trigPin on HIGH state for 10 micro seconds
  digitalWrite(trigPinFran, HIGH);
  delayMicroseconds(10);
  digitalWrite(trigPinFran, LOW);
  
  // Reads the echoPin, returns the sound wave travel time in microseconds
  durationFran = pulseIn(echoPinFran, HIGH);
  
  // Calculating the distance
  distanceFran = durationFran*0.034/2;

  float a = 0;
  
  if(distanceMati <= 50){
    a += distanceMati;
  }

  if(distanceFran <= 50){
    a += distanceFran;
  }

   //if(a <= 100){
   a = sin(2*pi*i/10);
      Serial.println(a);
      
    //sin(2*pi*a*i/1000
  //}
  
  i += 1;
}
