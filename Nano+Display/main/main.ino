// include the library code:
#include <LiquidCrystal.h> // initialize the library with the numbers of the interface pins - The numbers
//are the pin connected in sequence from RS to DB7
LiquidCrystal lcd(7, 6, 5, 4, 3, 2);
 
void setup() 
{
    // set up the LCD's number of columns and rows:
   lcd.begin(16, 2);
   // Print a message to the LCD
   lcd.print("hello, world!");
 
}void loop() {
     // set the cursor to column 0, line 1
    // (note: line 1 is the second row, since counting begins with 0):
   lcd.setCursor(0, 1);
    // print the number of seconds since reset:
   lcd.print("tataylino.com");
}
